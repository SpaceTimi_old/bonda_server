<?php

include_once __APP_SRC_DIR . 'ChangeEventHandlers/UserCountryChangeEventHandler.php';

class AppChangeEventHandlers {

    public static function Register() {
        $handlers = array(
            new UserCountryChangeEventHandler()
        );
        ChangeEventHandler::RegisterChangeEventHandlers($handlers);
    }
}

?>


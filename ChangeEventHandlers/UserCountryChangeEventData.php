<?php

class UserCountryChangeEventData {

    /** @var string */
    public $oldCountry;

    /** @var string */
    public $newCountry;
}

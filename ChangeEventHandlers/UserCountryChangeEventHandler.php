<?php

include_once __APP_SRC_DIR . 'ChangeEventHandlers/UserCountryChangeEventData.php';

class UserCountryChangeEventHandler implements IChangeEventHandler {

    /**
     * @return string
     */
    function GetChangeEventType() {
        return 'userCountryChangeEvent';
    }

    /**
     * @param $changeEventData UserCountryChangeEventData
     */
    function HandleChangeEvent($changeEventData) {
        Log::LogMessage('old country: ' . $changeEventData->oldCountry);
        Log::LogMessage('new country: ' . $changeEventData->newCountry);
    }
}

<?php

include_once __APP_SRC_DIR . "Tables/AppConstantList.php";
include_once __APP_SRC_DIR . "Tables/PlanetList.php";
include_once __APP_SRC_DIR . "Tables/CountryList.php";
include_once __APP_SRC_DIR . "Tables/StarList.php";

AppConstantList::GetInstance();
PlanetList::GetInstance();
CountryList::GetInstance();
StarList::GetInstance();

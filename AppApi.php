<?php

include_once __APP_SRC_DIR . "Data/CountryListDataModel.php";
include_once __APP_SRC_DIR . "Data/AppConstantsDataModel.php";
include_once __APP_SRC_DIR . "Data/StarListDataModel.php";
include_once __APP_SRC_DIR . "Data/User/UserCountryDataModel.php";

/**
 * @return DataModelBase[]
 */
function GetAppDataModels() {
    $dataModels = array();

    {
        $countryListData = new CountryListDataModel();
        $countryListData->prepareData();
        $dataModels[] = $countryListData;
    }
    {
        $appConstantsData = new AppConstantsDataModel();
        $appConstantsData->prepareData();
        $dataModels[] = $appConstantsData;
    }
    {
        $starListData = new StarListDataModel();
        $starListData->prepareData();
        $dataModels[] = $starListData;
    }

    return $dataModels;
}

/**
 * @return DataModelBase[]
 */
function GetAppUserDataModels() {
    $dataModels = array();

    {
        $userCountryData = new UserCountryDataModel();
        $userCountryData->prepareData();
        $dataModels[] = $userCountryData;
    }

    return $dataModels;
}




?>


<?php

include_once __APP_SRC_DIR . "Tables/UserCountry.php";

class UserCountryDataModel extends DataModelBase {
    public $country_id;

    function prepareData() {
        $userId = Context::GetContext()->user->user_id;
        $userCountry = UserCountry::loadByUserId($userId);
        if (empty($userCountry)) {
            $userCountry = UserCountry::createNew($userId, AppConstantList::GetInstance()->GetDefaultCountryId());
        }
        $this->country_id = $userCountry->country_id;
    }

    #region IDataModelDescriptor
    /**
     * @return string[]
     */
    public function GetDependencyTableNames() {
        return array(DataMapper::GetDataMapper(UserCountry::GetClassName())->tableName);
    }

    /**
     * @return bool
     */
    public function IsUserDataModel() {
        return true;
    }

    /**
     * @return int
     */
    public function GetAppId() {
        return AppIds::$BONDA_APP_ID;
    }
    #endregion
}
?>

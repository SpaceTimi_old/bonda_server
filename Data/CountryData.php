<?php

class CountryData {
    /** @var int */
    public $id;

    /** @var string */
    public $common_name;

    /** @var float */
    public $latitude;

    /** @var float */
    public $longitude;

    function __construct($id_, $common_name_, $latitude_, $longitude_) {
        $this->id = $id_;
        $this->common_name = $common_name_;
        $this->latitude = $latitude_;
        $this->longitude = $longitude_;
    }

}

?>
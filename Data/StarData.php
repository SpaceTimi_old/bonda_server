<?php

class StarData {
    /** @var int */
    public $id;

    /** @var string */
    public $common_name;

    /** @var float */
    public $right_ascension;

    /** @var float */
    public $declination;

    /** @var float */
    public $distance_in_parsecs;

    /** @var float */
    public $apparent_magnitude;

    /** @var float */
    public $absolute_magnitude;

    /** @var string */
    public $constellation_name;

    /** @var float */
    public $luminosity;


    function __construct($id, $common_name, $right_ascension, $declination, $distance_in_parsecs,
                         $apparent_magnitude, $absolute_magnitude, $constellation_name, $luminosity) {

        $this->id = $id;
        $this->common_name = $common_name;
        $this->right_ascension = $right_ascension;
        $this->declination = $declination;
        $this->distance_in_parsecs = $distance_in_parsecs;
        $this->apparent_magnitude = $apparent_magnitude;
        $this->absolute_magnitude = $absolute_magnitude;
        $this->constellation_name = $constellation_name;
        $this->luminosity = $luminosity;
    }


}

?>
<?php

include_once __APP_SRC_DIR . "Data/CountryData.php";
include_once __APP_SRC_DIR . "Tables/CountryList.php";

class CountryListDataModel extends DataModelBase {
    /** @var CountryData[] */
    public $countries;

    public function prepareData() {
        $countries = CountryList::GetInstance()->GetCountries();
        /** @var Country $country */
        foreach ($countries as $country) {
            $countryMessage = new CountryData($country->id, $country->common_name, $country->latitude, $country->longitude);
            $this->countries[] = $countryMessage;
        }
    }

    #region IDataModelDescriptor
    /**
     * @return string[]
     */
    public function GetDependencyTableNames() {
        return array(DataMapper::GetDataMapper(Country::GetClassName())->tableName);
    }

    /**
     * @return bool
     */
    public function IsUserDataModel() {
        return false;
    }

    /**
     * @return int
     */
    public function GetAppId() {
        return AppIds::$BONDA_APP_ID;
    }
    #endregion
}

?>

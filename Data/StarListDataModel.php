<?php

include_once __APP_SRC_DIR . "Tables/StarList.php";
include_once __APP_SRC_DIR . "Tables/AppConstantList.php";
include_once __APP_SRC_DIR . "Data/StarData.php";

class StarListDataModel extends DataModelBase {
    /** @var StarData[] */
    public $stars;

    public function prepareData() {
        $stars = StarList::GetInstance()->GetStars();
        $apparentMagnitudeCutoff = AppConstantList::GetInstance()->GetStarsApparentMagnitudeCutoff();

        /** @var Star $star */
        foreach ($stars as $star) {
            if ($star->apparent_magnitude <= $apparentMagnitudeCutoff) {
                $this->stars[] = new StarData($star->id, $star->common_name, $star->right_ascension,
                    $star->declination, $star->distance_in_parsecs, $star->apparent_magnitude,
                    $star->absolute_magnitude, $star->constellation_name, $star->luminosity);
            }
        }
    }

    #region IDataModelDescriptor
    /**
     * @return string[]
     */
    public function GetDependencyTableNames() {
        return array(DataMapper::GetDataMapper(Star::GetClassName())->tableName,
                     DataMapper::GetDataMapper(AppConstant::GetClassName())->tableName);
    }

    /**
     * @return bool
     */
    public function IsUserDataModel() {
        return false;
    }

    /**
     * @return int
     */
    public function GetAppId() {
        return AppIds::$BONDA_APP_ID;
    }
    #endregion
}

?>

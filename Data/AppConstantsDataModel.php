<?php

include_once __APP_SRC_DIR . "Tables/AppConstantList.php";

class AppConstantsDataModel extends DataModelBase {

    /** @var float */
    public $earthAxisTiltDegrees;

    /** @var float */
    public $starfieldDistanceKilometers;

    /** @var float */
    public $unityUnitsToKilometers;

    /** @var float */
    public $starsApparentMagnitudeCutoff;

    public function prepareData() {
        $this->earthAxisTiltDegrees = AppConstantList::GetInstance()->GetEarthAxisTiltDegrees();
        $this->starfieldDistanceKilometers = AppConstantList::GetInstance()->GetStarfieldDistanceKilometers();
        $this->unityUnitsToKilometers = AppConstantList::GetInstance()->GetUnityUnitsToKilometers();
        $this->starsApparentMagnitudeCutoff = AppConstantList::GetInstance()->GetStarsApparentMagnitudeCutoff();
    }

    #region IDataModelDescriptor
    /**
     * @return string[]
     */
    public function GetDependencyTableNames() {
        return array(DataMapper::GetDataMapper(AppConstant::GetClassName())->tableName);
    }

    /**
     * @return bool
     */
    public function IsUserDataModel() {
        return false;
    }

    /**
     * @return int
     */
    public function GetAppId() {
        return AppIds::$BONDA_APP_ID;
    }
    #endregion
}


?>

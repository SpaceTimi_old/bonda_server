<?php

include_once __SHARED_SRC_DIR."Core/RedisObjectListBase.php";
include_once __APP_SRC_DIR . "Tables/Country.php";

class CountryList extends RedisObjectListBase {

    /** @var CountryList */
    private static $_instance;

    /**
     * @return CountryList
     */
    public static function GetInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new CountryList();
        }
        return self::$_instance;
    }

    public function GetCountries() {
        return $this->getList();
    }

    /**
     * @param $id string
     * @return Country|null
     */
    public function GetCountryById($id) {
        $list = $this->getList();
        /** @var Country $country */
        foreach ($list as $country) {
            if ($country->id == $id) {
                return $country;
            }
        }
        return null;
    }

    /**
     * @return DataMapper
     */
    protected function getDataMapper() {
        return DataMapper::GetDataMapper(Country::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}
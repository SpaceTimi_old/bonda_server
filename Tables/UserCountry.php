<?php

include_once __SHARED_SRC_DIR."Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class UserCountry extends RedisObjectBase {

    /** @var int */
    public $user_id;
    /** @var string */
    public $country_id;

    /**
     * @param $userId string
     * @return UserCountry
     */
    public static function loadByUserId($userId) {
        $userCountry = new UserCountry();
        RedisObjectBase::loadByPrimaryKeys(array($userId), $userCountry);

        return $userCountry;
    }

    /**
     * @param $userId int
     * @param $countryId string
     * @return UserCountry
     */
    public static function createNew($userId, $countryId) {
        $userCountry = new UserCountry();
        $userCountry->user_id = $userId;
        $userCountry->country_id = $countryId;
        $userCountry->Apply();

        return $userCountry;
    }

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}

DataMapper::AddDataMapper(UserCountry::GetClassName(),
    new DataMapper(__APP_DATABASE,
        'user_country',
        true,
        UserCountry::GetClassName(),
        array('user_id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_API ||
                    Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));
?>

<?php

include_once __SHARED_SRC_DIR."Core/RedisObjectListBase.php";
include_once __APP_SRC_DIR . "Tables/Planet.php";

class PlanetList extends RedisObjectListBase {

    /** @var PlanetList */
    private static $_instance;

    /**
     * @return PlanetList
     */
    public static function GetInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new PlanetList();
        }
        return self::$_instance;
    }

    public function GetPlanets() {
        return $this->getList();
    }

    public function GetPlanetByName($name) {
        $list = $this->getList();
        /** @var Planet $planet */
        foreach ($list as $planet) {
            if ($planet->common_name == $name) {
                return $planet;
            }
        }
    }

    /**
     * @return DataMapper
     */
    protected function getDataMapper() {
        return DataMapper::GetDataMapper(Planet::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}
<?php

include_once __SHARED_SRC_DIR."Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class Planet extends DBObjectBase {

    public $id;
    public $common_name;
    public $axis_tilt;

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }
}

DataMapper::AddDataMapper(Planet::GetClassName(),
new DataMapper(__APP_DATABASE,
        'meta_planet',
        false,
        Planet::GetClassName(),
        array('id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));
?>

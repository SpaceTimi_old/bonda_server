<?php

include_once __SHARED_SRC_DIR."Core/RedisObjectListBase.php";
include_once __APP_SRC_DIR . "Tables/AppConstant.php";

class AppConstantList extends RedisObjectListBase {

    const kEarthAxisTiltDegrees = 'earthAxisTiltDegrees';
    const kStarfieldDistanceKilometers = 'starfieldDistanceKilometers';
    const kUnityUnitsToKilometers = 'unityUnitsToKilometers';
    const kStarsApparentMagnitudeCutoff = 'starsApparentMagnitudeCutoff';
    const kDefaultCountryId = 'defaultCountryId';

    /** @var AppConstantList */
    private static $_instance;

    /**
     * @return AppConstantList
     */
    public static function GetInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new AppConstantList();
        }
        return self::$_instance;
    }


    #region App Constants
    /**
     * @return float
     */
    public function GetEarthAxisTiltDegrees() {
        return (float)$this->GetAppConstantValueById(self::kEarthAxisTiltDegrees);
    }

    /**
     * @return float
     */
    public function GetStarfieldDistanceKilometers() {
        return (float)$this->GetAppConstantValueById(self::kStarfieldDistanceKilometers);
    }

    /**
     * @return float
     */
    public function GetUnityUnitsToKilometers() {
        return (float)$this->GetAppConstantValueById(self::kUnityUnitsToKilometers);
    }

    /**
     * @return float
     */
    public function GetStarsApparentMagnitudeCutoff() {
        return (float)$this->GetAppConstantValueById(self::kStarsApparentMagnitudeCutoff);
    }

    /**
     * @return string
     */
    public function GetDefaultCountryId() {
        return $this->GetAppConstantValueById(self::kDefaultCountryId);
    }
    #endregion

    /**
     * @param $id string
     * @return string
     */
    private function GetAppConstantValueById($id) {
        $list = $this->getList();
        /** @var AppConstant $appConstant */
        foreach ($list as $appConstant) {
            if ($appConstant->id == $id) {
                return $appConstant->value;
            }
        }
        return null;
    }

    /**
     * @return DataMapper
     */
    protected function getDataMapper() {
        return DataMapper::GetDataMapper(AppConstant::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}
<?php

include_once __SHARED_SRC_DIR."Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class Star extends DBObjectBase {

    public $id;
    public $common_name;
    public $right_ascension;
    public $declination;
    public $distance_in_parsecs;
    public $apparent_magnitude;
    public $absolute_magnitude;
    public $constellation_name;
    public $luminosity;

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }
}

DataMapper::AddDataMapper(Star::GetClassName(),
    new DataMapper(__APP_DATABASE,
        'meta_star',
        false,
        Star::GetClassName(),
        array('id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));

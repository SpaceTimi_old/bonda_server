<?php

include_once __SHARED_SRC_DIR."Core/RedisObjectListBase.php";
include_once __APP_SRC_DIR . "Tables/Star.php";

class StarList extends RedisObjectListBase {

    /** @var StarList */
    private static $_instance;

    /**
     * @return StarList
     */
    public static function GetInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new StarList();
        }
        return self::$_instance;
    }

    public function GetStars() {
        return $this->getList();
    }

    /**
     * @return DataMapper
     */
    protected function getDataMapper() {
        return DataMapper::GetDataMapper(Star::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}
<?php

include_once __SHARED_SRC_DIR."Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class Country extends DBObjectBase {

    public $id;
    public $common_name;
    public $latitude;
    public $longitude;

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }
}

DataMapper::AddDataMapper(Country::GetClassName(),
new DataMapper(__APP_DATABASE,
        'meta_country',
        false,
        Country::GetClassName(),
        array('id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));

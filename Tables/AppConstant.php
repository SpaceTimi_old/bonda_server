<?php

include_once __SHARED_SRC_DIR."Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class AppConstant extends DBObjectBase {

    public $id;
    public $value;

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }
}

DataMapper::AddDataMapper(AppConstant::GetClassName(),
    new DataMapper(__APP_DATABASE,
        'meta_appconstant',
        false,
        AppConstant::GetClassName(),
        array('id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));
